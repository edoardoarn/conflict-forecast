library(feather)

orig_path <- "E:/Datasets/conflict-forecast/"
dest_path <- "E:/Projects/_LINKS/conflict-forecast/data/"
filename <- "data_clean_27-07"

read_path <- paste(orig_path, filename, ".Rda", sep = "")
load(read_path, verbose = TRUE)
df <- as.data.frame(df)
print(df)

write_path <- paste(dest_path, filename, ".feather", sep = "")
print(write_path)

write_feather(df, write_path)
system.time(read_feather(write_path))

print("Done!")