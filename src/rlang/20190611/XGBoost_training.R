library(haven)
library(dplyr)
library(rworldmap)

source('lib/lib_preprocess.R')
source('lib/lib_models.R')

set.seed(414)

KNTREE <- 100
# dd_clean <- read_dta("resources-conflict/Data/data_clean.dta")
dd_clean <- read_dta("resources-conflict/Data/data_clean_interp.dta")

dd_clean <- as.data.frame(dd_clean)
colnames(dd_clean)

# visto la grande quantità di dati, ho selezionato un'area geografica per cui
# eseguire dei primi esperimenti
# south asia
country <- c('Afghanistan', 'Bangladesh', 'Bhutan','India', 
             'Maldives', 'Nepal', 'Pakistan', 'Sri Lanka')

countr.id <- c('2', '19', '26','103', 
               '135', '153', '166', '207')

df <- dd_clean[dd_clean$country %in% countr.id, ]
summary(df)

# nel dataset sono presenti alcune righe con valori di polazione NA
# check su questa area geografica per verficare dove sono localizzati i 
# valori NA di popolazione
newmap <- getMap(resolution = "low")
europe <- newmap[which(newmap$REGION=="Asia"),] 
plot(europe, asp = 1)
points(df$xcoord, df$ycoord, col = "red", cex = .6)
points(df[is.na(df$pop), ]$xcoord, df[is.na(df$pop), ]$ycoord, 
       col = "blue", cex = 1.6)

#in questo caso corrispondono ad un unico punto, quindi per ora ho pensato 
# di tralasciare queste righe di dati; 
df <- df[!is.na(df$pop), ]


# selziono i dati fino al 2017 (anno per cui abbiamo i conflitti)
df <- df[df$year <= 2017, ]


df.new <- SpatialLagFunction(df)

# ho creato una funzione (salvata in lib_models.R) che per ogni variabile 
# sostituisce i valori NA di una cella con la media dello specifico country 
# in quell'anno  
# da riscrivere meglio con sapply
df1 <- remove.Na.value('crop', df)
df1 <- remove.Na.value('temp_mean', df1)
df1 <- remove.Na.value('temp_anom', df1)
df1 <- remove.Na.value('pop_rural', df1)
df1 <- remove.Na.value('educated_people', df1)
df1 <- remove.Na.value('food_supply', df1)
df1 <- remove.Na.value('trade_food', df1)
df1 <- remove.Na.value('food_importer', df1)
df1 <- remove.Na.value('undernour_people', df1)
df1 <- remove.Na.value('agric_value_prod', df1)
df1 <- remove.Na.value('imports_value', df1)
df1 <- remove.Na.value('exports_value', df1)
df1 <- remove.Na.value('temperature', df1)

# table conflitti fino al 2017
table(df1$conf_onset)
#     0       1 
#  130767   1065 

#  calcolo la somma cumualtiva di conf_onset in modo tale da avere un training set
# e un test set bilanciato 
table_onset <- as.data.frame(as.matrix(table(df1$year, df1$conf_onset)))
table_onset$Var2 <- as.numeric(as.character(table_onset$Var2))
table_onset <- table_onset[table_onset$Var2 == 1, ]
table_onset$cum_sum <- cumsum(table_onset$Freq)

df1$conf_onset <- as.factor(df1$conf_onset)
trainset <- df1[df1$year <= 2007, ]
testset <- df1[df1$year > 2007, ]

# seleziono le variabili da introdurre nel modello
# al momento ci sono ancora delle variabili con valori NA 
# che però sarebbe interessante introdurre nel modello
col.selected <- c("conf_onset", "year", "country", #"lagconf_onset", # "lagconf_incid",
                  # "incid1", "incid2", "incid3", 
                  "gdp", "gdp_pc", "pop",  "temperature", #"regionid_gadm",
                  "precipitation", "crop", "temp_anom", "prec_anom", #"lagcrop", 
                  "pop_rural", "educated_people", "food_supply", "trade_food",
                  "food_importer", "undernour_people", "agric_value_prod",
                  "imports_value", "exports_value", "year50s", "year70s", 
                  "year60s", "year80s", "year90s", 'year20s')

trainSelected <- trainset[, col.selected]
testsetSelected <- testset[, col.selected]
class(df1$conf_onset)

# Transform the two data sets into xgb.Matrix
xgb.dataset <- XGBoostDataset(trainSelected, testsetSelected) 

# Define the parameters for classification
num_class = length(levels(df1$conf_onset))
params = list(
  booster="gbtree",
  eta = 0.001,
  max_depth = 50,
  gamma = 3,
  subsample = 0.75,
  colsample_bytree = 1,
  objective = "binary:logistic",
  eval_metric = c("auc")#,
  # num_class = 2
  )


xgb.1 <- XGBoostFucntion(xgb.dataset[['xgb.train']], xgb.dataset[['xgb.test']])
  
importance_frame <- xgb.importance(colnames(train.data[!'conf_onset']),  model = xgb.1[['model']])
xgb.plot.importance(importance_frame)

col.sel.pruned <- importance_frame[importance_frame$Importance > quantile(importance_frame$Importance)[2], 'Feature']

xgb.dataset <- XGBoostDataset(trainSelected, testsetSelected) 







